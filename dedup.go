package main

import (
	"context"
	"encoding/json"
	"fmt"
	"image"
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"

	"github.com/corona10/goimagehash"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	sheets "google.golang.org/api/sheets/v4"
)

var spreadsheetId = "12KnIIyvkud_ZEIhLvusUNXIeCFwtm-etdSPJ5SKj3eE"

func main() {
	jsonConfig, err := ioutil.ReadFile("./credentials.json")
	if err != nil {
		log.Fatal("Loading creds:", err)
	}
	config, err := google.ConfigFromJSON(jsonConfig,
		"https://www.googleapis.com/auth/photoslibrary",
		"https://www.googleapis.com/auth/spreadsheets")
	if err != nil {
		log.Fatal("Reading json config:", err)
	}

	ctx := context.Background()

	tokFile := "token.json"

	var token *oauth2.Token

	if f, err := os.Open(tokFile); err == nil {
		json.NewDecoder(f).Decode(&token)
	}

	if token == nil {
		fmt.Println("Visit this link:", config.AuthCodeURL(""))

		fmt.Print("Enter token: ")
		var authCode string
		if _, err := fmt.Scan(&authCode); err != nil || len(authCode) == 0 {
			log.Fatal("Error reading authorization code: ", err)
		}

		token, err = config.Exchange(ctx, authCode)
		if err != nil {
			log.Fatal("Error getting token: ", err)
		}
		f, err := os.OpenFile(tokFile, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
		if err != nil {
			log.Fatal("Error saving token: ", err)
		}
		json.NewEncoder(f).Encode(token)
		f.Close()
	}

	client := config.Client(ctx, token)
	shs, err := sheets.New(client)
	if err != nil {
		log.Fatal("Error creating Sheets client: ", err)
	}
	pageToken := ""
	for {
		request, err := http.NewRequest("GET", "https://photoslibrary.googleapis.com/v1/mediaItems?pageSize=100&pageToken="+url.QueryEscape(pageToken), nil)
		if err != nil {
			log.Fatal("Error making request: ", err)
		}
		token.SetAuthHeader(request)
		resp, err := client.Do(request)
		if err != nil {
			log.Fatal("Error getting media items: ", err)
		}
		body, err := ioutil.ReadAll(resp.Body)
		resp.Body.Close()
		if resp.StatusCode/100 != 2 {
			log.Fatal("Bad status: ", resp.Status)
		}
		photoItems := struct {
			NextPageToken string `json:"nextPageToken"`
			MediaItems    []struct {
				Id            string `json:"id"`
				Filename      string `json:"filename"`
				MimeType      string `json:"mimeType"`
				BaseURL       string `json:"baseUrl"`
				MediaMetadata struct {
					CreationTime string `json:"creationTime"`
					Width        string `json:"width"`
					Height       string `json:"height"`
				} `json:"mediaMetadata"`
			} `json:"mediaItems"`
		}{}
		err = json.Unmarshal(body, &photoItems)
		if err != nil {
			log.Fatal("Error unmarshaling body: ", err)
		}
		pageToken = photoItems.NextPageToken
		if pageToken == "" || len(photoItems.MediaItems) == 0 {
			break
		}
		for _, item := range photoItems.MediaItems {
			fmt.Println(item.BaseURL)
			request, err := http.NewRequest("GET", item.BaseURL, nil)
			if err != nil {
				log.Print("Error making request: ", err)
				continue
			}
			token.SetAuthHeader(request)
			resp, err := client.Do(request)
			if err != nil {
				log.Print("Error getting image: ", err)
				continue
			}
			if resp.StatusCode/100 != 2 {
				resp.Body.Close()
				log.Print("Bad status getting image: ", resp.Status)
				continue
			}
			phash, ahash, dhash, err := hashImage(resp.Body)
			resp.Body.Close()
			if err != nil {
				log.Print("Error hashing image: ", err)
				continue
			} else {
				log.Printf("HASH", phash, ahash, dhash)
				_, err := shs.Spreadsheets.Values.Append(spreadsheetId, "A1", &sheets.ValueRange{
					Range: "A1",
					Values: [][]interface{}{
						{item.Id, item.BaseURL, item.MediaMetadata.Width, item.MediaMetadata.Height, phash, ahash, dhash},
					},
				}).ValueInputOption("RAW").Do()
				if err != nil {
					log.Print("Error updating spreadsheet: ", err)
					continue
				}
			}
		}
	}
}

func hashImage(imageData io.Reader) (string, string, string, error) {
	img, _, err := image.Decode(imageData)
	if err != nil {
		return "", "", "", err
	}

	phsh, err := goimagehash.PerceptionHash(img)
	if err != nil {
		return "", "", "", err
	}
	ahsh, err := goimagehash.AverageHash(img)
	if err != nil {
		return "", "", "", err
	}
	dhsh, err := goimagehash.DifferenceHash(img)
	if err != nil {
		return "", "", "", err
	}
	return phsh.ToString(), ahsh.ToString(), dhsh.ToString(), nil
}
