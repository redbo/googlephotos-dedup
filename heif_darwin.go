package main

/*
This uses a little cgo/objective-c and OSX core libraries to load heif
images (inefficiently, with a bunch of buffer copies, etc.).

TODO: support other operating systems.

There's an heif library for go [1], but getting the libraries it relies
on compiled and installed is burdensome.  Heif is good for image sizes and
hardware acceleration, but not for ease of implementation.

1. https://github.com/strukturag/libheif/tree/master/go/heif
*/

/*
#cgo CFLAGS: -x objective-c
#cgo LDFLAGS: -framework CoreImage -framework Foundation -framework CoreGraphics
#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <CoreImage/CoreImage.h>

void *ImagePixelData(void *buf, int buflen, int *width, int *height) {
  	@autoreleasepool {
	  	// CIImage supports loading heif images from a buffer.
		NSData *imgData = [NSData dataWithBytes:buf length:buflen];
		CIImage *image = [CIImage imageWithData:imgData];
		if (image == NULL) {
			return NULL;
		}
		CGColorSpaceRef cs = CGColorSpaceCreateDeviceRGB();
		CIContext *ctx = [CIContext context];
		// CIImage is just a sort of logical drawing surface, so we need to render it to an
		// RGBA8 CGImage to get actual pixels in a color space we can use.
		struct CGImage *cimg = [ctx createCGImage:image fromRect:image.extent format:kCIFormatRGBA8 colorSpace:cs];
		*width = (int)CGImageGetWidth(cimg);
		*height = (int)CGImageGetHeight(cimg);
		// grab the raw pixel data and copy it to some malloc'ed memory.
		CFDataRef rawData = CGDataProviderCopyData(CGImageGetDataProvider(cimg));
		CGImageRelease(cimg);
		CGColorSpaceRelease(cs);
		void *dst = malloc(CFDataGetLength(rawData));
		memcpy(dst, CFDataGetBytePtr(rawData), CFDataGetLength(rawData));
		CFRelease(rawData);
		return dst;
	}
}
*/
import "C"
import (
	"errors"
	"image"
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"
	"io"
	"io/ioutil"
	"unsafe"
)

func init() {
	image.RegisterFormat("heic", "????ftyp", func(r io.Reader) (image.Image, error) {
		data, err := ioutil.ReadAll(r)
		if err != nil {
			return nil, err
		}
		var width, height C.int
		pixeldata := C.ImagePixelData(unsafe.Pointer(&data[0]), C.int(len(data)), &width, &height)
		if pixeldata == C.NULL {
			return nil, errors.New("Error loading HEIC image")
		}
		img := &image.RGBA{
			Rect:   image.Rect(0, 0, int(width), int(height)),
			Pix:    C.GoBytes(unsafe.Pointer(pixeldata), width*height*4),
			Stride: 4 * int(width),
		}
		C.free(pixeldata)
		return img, nil
	}, func(io.Reader) (image.Config, error) { return image.Config{}, errors.New("Unimplemented") })
}
